 class carousel{
  /**
   * 
   * @param {elementHTML} element 
   * @param {object} options 
   * @param {object} options.slidesToScroll=nombre d'elements a scroller
   * @param {object} options.slidesVisible=nombre d'element visible dans un slide
   */
  constructor(element,options={}){
    this.element=element
    this.options= Object.assign({},{
      slidesToScroll:1,
      slidesVisible:1
    },options)
    this.children= [].slice.call(element.children) //pour ne garder que les enfants existant au moment de l'execution
    this.currentItem = 0
    this.root=this.createDivWithclass('carousel')
    this.container=this.createDivWithclass('carousel__container')
    let ratio= this.children.length / this.options.slidesVisible
    this.container.style.width= (ratio * 100) + "%" //166.66% 
    this.root.appendChild(this.container)
    this.element.appendChild(this.root)
    this.children.forEach((child)=> {
      let item = this.createDivWithclass('carousel__item')
      item.style.width= ((100 / this.options.slidesVisible) / ratio) + "%" //"20%"
      item.appendChild(child)
      this.container.appendChild(item) 
    });
    this.createnavigation()
    this.setTimer()
  }
  createnavigation(){
    let nextButton=this.createDivWithclass('carousel__next')
    let prevButton=this.createDivWithclass('carousel__prev')
    this.root.appendChild(nextButton)
    this.root.appendChild(prevButton)
    nextButton.addEventListener('click',this.next.bind(this))
    prevButton.addEventListener('click',this.prev.bind(this))
  }
  setTimer() {
    this.timer = null
     this.timer = setInterval(() => {
         this.next()
     },8000)  
 }
  next(){
    this.goTo(this.currentItem + this.options.slidesToScroll)
    
  }
  prev(){
    this.goTo(this.currentItem - this.options.slidesToScroll)
  }
  
  /**
   * deplace le carousel vers l'element ciblé
   * @param {number} index 
   */
  goTo(index){
    if(index < 0){
      index = this.children.length - this.options.slidesVisible
    }else if (index >= this.children.length || this.children[this.currentItem + this.options.slidesVisible]=== undefined){
      index=0
    }
    let translateX =  index * -100/ this.children.length 
    this.container.style.transform ='translate3d( '+ translateX +'%, 0 ,0)'
    this.container.style.transition = "3s"
    this.currentItem=index
    
  }
  /**
   * 
   * @param {string} className 
   * @returns {HTMLElement}
   */
  createDivWithclass(className){
    let div=document.createElement('div')
    div.setAttribute('class',className)
    return div
  }
  
}
  


  new carousel(document.querySelector("#carousel1"),{
    slidesToScroll:1,
    slidesVisible:3
  })
 
  



